import React, { Component } from "react";
import "./App.css";
import Person from "./Person/Person";

// class component
class App extends Component {
  state = {
    persons: "aya",
    personsArr: [
      { id: 1, name: "aya" },
      { id: 2, name: "ali" },
      { id: 3, name: "ahmed" },
    ],
    showPersons: true,
  };
  changePersonHandler = (newName) => {
    this.setState({
      persons: newName,
    });
  };
  nameChangedHandler = (event) => {
    this.setState({
      persons: event.target.value,
    });
  };
  nameChangedArrHandler = (event, id) => {
    const personIndex = this.state.personsArr.findIndex((p) => {
      //find person index in personsArr
      return p.id === id;
    });
    const person = {
      //get person from array then assign it to new person object
      ...this.state.personsArr[personIndex],
    };
    // or
    //const person = Object.assign({}, this.state.personsArr[personIndex]);

    person.name = event.target.value; //change value of object name
    const persons = [...this.state.personsArr]; //change state immutable
    persons[personIndex] = person;
    this.setState({ personsArr: persons });
  };

  render() {
    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <div>
          <Person
            name={this.state.persons}
            click={this.changePersonHandler.bind(this, "Ali")}
            changed={this.nameChangedHandler}
          >
            this is person component
          </Person>
        </div>
      );
    }
    return (
      <div className="App">
        <h1>This is React App</h1>
        {/* react Component & event */}
        {/* <Person
          name={this.state.persons}
          click={this.changePersonHandler.bind(this, "Ali")}
        /> */}

        {/* conditional rendering in layout */}
        {/* {this.state.showPersons ? (
          <Person
            name={this.state.persons}
            click={this.changePersonHandler.bind(this, "Ali")}
            changed={this.nameChangedHandler}
          >
            this is person component
          </Person>
        ) : null} */}

        {/*conditional rendering by function */}
        {/* {persons} */}

        {/* Listing */}
        {this.state.personsArr.map((person, index) => (
          <Person
            key={person.id}
            name={person.name}
            changed={(event) => this.nameChangedArrHandler(event, person.id)}
          />
        ))}
      </div>
    );
  }
}
// functional component
// function App() {
//   return (
//     <div className="App">
//       <h1>This is React App</h1>
//       <Person name="aya" />
//     </div>
//   );
//   // return React.createElement(
//   //   "div",
//   //   { className: "App" },
//   //   React.createElement("h1", null, "This is React App")
//   // );
//   //return React.createElement("h1", {}, "I do not use JSX!");
// }

export default App;
